﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class PoliedroMenuScript : MonoBehaviour
{
    public PoliedrosEnum poliedro;
    public Transform trf
    {
        get
        {
            trf = trf == null ? transform : trf;
            return trf;
        }
        private set { trf = value; }
    }

}
