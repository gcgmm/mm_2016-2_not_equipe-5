﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{

    public Transform[] poliedros;

    public Transform label;

    private int selectedShapeId;
    private Transform selectedShapeTrf;

    private float lastChange;
    private float changeDelay = 0.33f;

    private bool loadScene = false;

    public PoliedrosEnum poliedroSelecionado { private set; get; }

    void Awake()
    {
        selectedShapeTrf = poliedros[0];

        poliedroSelecionado = poliedros[0].GetComponent<PoliedroMenuScript>().poliedro;
        label.GetComponent<TextMesh>().text = poliedroSelecionado.ToString();

        DontDestroyOnLoad(gameObject);
    }

    public void ChangeShape(bool back)
    {
        //labels[selectedShapeId].gameObject.SetActive(false);



        selectedShapeId = (selectedShapeId + 1) % poliedros.Length;

        selectedShapeTrf = poliedros[selectedShapeId];

        poliedroSelecionado = poliedros[selectedShapeId].GetComponent<PoliedroMenuScript>().poliedro;

        label.GetComponent<TextMesh>().text = poliedroSelecionado.ToString();

        //labels[selectedShapeId].gameObject.SetActive(true);
    }

    void Update()
    {
        if (!loadScene)
        {
            selectedShapeTrf.RotateAround(CalculateCentroid(selectedShapeTrf), Vector3.up, 50f * Time.deltaTime);

            if ((changeDelay + lastChange < Time.time) && Input.GetAxis("Horizontal") != 0)
            {
                ChangeShape(Input.GetAxis("Horizontal") > 0);
                lastChange = Time.time;
            }

            if (Input.GetAxis("Submit") != 0 && !loadScene)
            {
                selectedShapeTrf.DOMove(Camera.main.transform.position, 0.5f).SetEase(Ease.InExpo).OnComplete(() => Submit());
            }
        }
    }

    private void Submit()
    {
        SceneManager.LoadScene("test");
        loadScene = true;
    }

    Vector3 CalculateCentroid(Transform trf)
    {
        if (trf && trf.root.gameObject == trf.gameObject)
        {
            Vector3 centroid = Vector3.zero;

            if (trf.childCount > 0)
            {
                Transform[] allChildren = trf.gameObject.GetComponentsInChildren<Transform>();
                foreach (Transform child in allChildren)
                {
                    centroid += child.transform.position;
                }
                centroid /= (allChildren.Length);
            }

            GameObject obj = new GameObject();
            obj.transform.parent = transform;
            obj.transform.position = centroid;

            return centroid;
        }
        return Vector3.zero;
    }
}
