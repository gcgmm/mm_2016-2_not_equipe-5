﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public enum PoliedrosEnum { Tetraedro, Hexaedro, Octaedro }

public class PoliedroScript : MonoBehaviour
{
    public float numArestas, numVertices, numFaces;
    public PoliedrosEnum poliedro;

    private Animator _animator;
    private Camera mainCam;
    private Transform _transform;
    private float abertura = 0;
    private Vector3 centroid;

    private Vector3 initpos;

    void Awake()
    {
        _transform = transform; // otimização
        _animator = GetComponent<Animator>();
        mainCam = Camera.main;

        _animator.speed = 0;
        _animator.Play("Anim", -1, 1);


        initpos = _transform.position;
        // mainCam.transform.LookAt(centroid);
        abertura = 1;
 
        Invoke("CalculateCentroid", 0.1f);
    }

    void Update()
    {

        float distanceFromCamera = Vector3.Distance(mainCam.transform.position, _transform.position);

        if (Input.GetAxis("RightJoyVertical") != 0)
        {
            if (distanceFromCamera >= 40)
            {
                mainCam.transform.Translate(Vector3.forward * Input.GetAxis("RightJoyVertical") * 400 * Time.deltaTime);
            }
            else
            {
                mainCam.transform.Translate(-Vector3.forward);
            }

            if (distanceFromCamera <= 100)
            {
                mainCam.transform.Translate(Vector3.forward * Input.GetAxis("RightJoyVertical") * 400 * Time.deltaTime);
            }
            else
            {
                mainCam.transform.Translate(Vector3.forward);
            }
        }

        if (Input.GetKey(KeyCode.Z))
        {
            if (Vector3.Distance(mainCam.transform.position, _transform.position) > 40)
                mainCam.transform.Translate(Vector3.forward * 50 * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.X))
        {
            if (Vector3.Distance(mainCam.transform.position, _transform.position) < 100)
                mainCam.transform.Translate(-Vector3.forward * 50 * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.Keypad0))
        {
            abertura = Mathf.Clamp01(abertura + 0.05f);
            MontarTudo(abertura);
        }
        else if (Input.GetKey(KeyCode.Space))
        {
            _transform.position = initpos;
            _transform.DOMove(mainCam.transform.position, 1.5f).SetEase(Ease.InExpo).SetDelay(1);
        }
        else if (Input.GetKey(KeyCode.Keypad1))
        {
            abertura = Mathf.Clamp01(abertura - 0.05f);
            MontarTudo(abertura);
        }
        else if (Input.GetAxis("Cancel") != 0)
        {
            Destroy(GameObject.Find("MenuManager"));
            SceneManager.LoadScene("SelectShape");
        }

        // trecho responsável pela planificação e solidificação
        if(Input.GetAxis("LeftTrigger") != 0)
        {
            abertura = Mathf.Clamp01(abertura + Input.GetAxis("LeftTrigger") * 0.5f);
            MontarTudo(abertura);
        }
        else if (Input.GetAxis("RightTrigger") != 0)
        {
            abertura = Mathf.Clamp01(abertura - Input.GetAxis("RightTrigger") * 0.5f);
            MontarTudo(abertura);
        }


        // trecho responsável pela rotação do objeto em relação a camera
        _transform.RotateAround(centroid, mainCam.transform.right, Input.GetAxis("Vertical") * 100 * Time.deltaTime);
        _transform.RotateAround(centroid, mainCam.transform.up, -Input.GetAxis("Horizontal") * 100 * Time.deltaTime);

    }

    private void CalculateCentroid()
    {
        if (transform.root.gameObject == transform.gameObject)
        {
            Vector3 centroid = Vector3.zero;

            if (transform.childCount > 0)
            {
                Transform[] allChildren = transform.gameObject.GetComponentsInChildren<Transform>();
                foreach (Transform child in allChildren)
                {
                    centroid += child.transform.position;
                }
                centroid /= (allChildren.Length);
            }

            GameObject obj = new GameObject();
            obj.transform.parent = transform;
            obj.transform.position = centroid;

            this.centroid = centroid;
        }
    }

    public void MontarTudo(float planificacao)
    {
        _animator.speed = 0;
        _animator.Play("Anim", -1, planificacao);
    }
}
